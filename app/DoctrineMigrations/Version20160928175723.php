<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160928175723 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(21) NOT NULL, password VARCHAR(60) NOT NULL, name VARCHAR(21) NOT NULL, last_name VARCHAR(21) NOT NULL, age INT NOT NULL, gender INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bonus (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(21) NOT NULL, wagering_multiplier INT NOT NULL, `trigger` INT NOT NULL, reward_type INT NOT NULL, reward_target_wallet INT NOT NULL, reward_value INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wallet (id INT AUTO_INCREMENT NOT NULL, player_id INT DEFAULT NULL, bonus_id INT DEFAULT NULL, currency INT NOT NULL, initial_value NUMERIC(19, 4) NOT NULL, current_value NUMERIC(19, 4) NOT NULL, status INT NOT NULL, INDEX IDX_7C68921F99E6F5DF (player_id), INDEX IDX_7C68921F69545666 (bonus_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wallet ADD CONSTRAINT FK_7C68921F99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE wallet ADD CONSTRAINT FK_7C68921F69545666 FOREIGN KEY (bonus_id) REFERENCES bonus (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE wallet DROP FOREIGN KEY FK_7C68921F99E6F5DF');
        $this->addSql('ALTER TABLE wallet DROP FOREIGN KEY FK_7C68921F69545666');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE bonus');
        $this->addSql('DROP TABLE wallet');
    }
}
