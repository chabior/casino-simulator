Casino simulator.
      
1. After cloning this repository in your terminal execute:       
   $ composer install          
  Script should:      
  - create full new database      
  - create two bonuses. One triggered on login with fixed amount of 10EUR and second one triggered on deposit with percentage of every deposited money.      
  - create two players.      
        
2. In order to use application point Virtual host to file:      
  web/app_dev.php      
  and access / webpage.