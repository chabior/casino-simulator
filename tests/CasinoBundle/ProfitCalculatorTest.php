<?php

namespace CasinoBundle\Tests;


use CasinoBundle\Entity\Bonus;
use CasinoBundle\Entity\BonusWallet;
use CasinoBundle\Entity\Player;
use CasinoBundle\Entity\Reward;
use CasinoBundle\Entity\Wallet;
use CasinoBundle\Enum\CurrencyTypeEnum;
use CasinoBundle\Enum\WalletStatusEnum;
use CasinoBundle\ProfitCalculator;
use PHPUnit\Framework\TestCase;

class ProfitCalculatorTest extends TestCase
{
    /**
     * @var ProfitCalculator
     */
    protected $profitCalculator;

    public function setUp()
    {
        $this->profitCalculator = new ProfitCalculator();
    }

    public function testExample()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 100);

        $bonusWallet = $this->createBonusWallet(10, 10, 10, 1);

        $player = new Player();
        $player->addWallet($realMoneyWallet);
        $player->addWallet($bonusWallet);

        $this->profitCalculator->calculate($player, $betAmount = 105, 0);

        $this->assertEquals(0, $realMoneyWallet->getCurrentValue());
        $this->assertEquals(5, $bonusWallet->getCurrentValue());

        $this->profitCalculator->calculate($player, $betAmount = 5, $winAmount = 20);
        $this->assertEquals(10, $realMoneyWallet->getCurrentValue());
        $this->assertEquals(10, $bonusWallet->getCurrentValue());

        $this->profitCalculator->calculate($player, $betAmount = 20, $winAmount = 30);
        $this->assertEquals(20, $realMoneyWallet->getCurrentValue());
        $this->assertEquals(10, $bonusWallet->getCurrentValue());
    }

    public function testOnlyRealMoneyWin()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 100);

        $player = new Player();
        $player->addWallet($realMoneyWallet);

        $this->profitCalculator->calculate($player, $betAmount = 100, $winAmount = 110);

        $this->assertEquals(110, $realMoneyWallet->getCurrentValue());
    }

    public function testOnlyRealMoneyLost()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 100);

        $player = new Player();
        $player->addWallet($realMoneyWallet);

        $this->profitCalculator->calculate($player, $betAmount = 50, $winAmount = 0);
        $this->assertEquals(50, $realMoneyWallet->getCurrentValue());

        $this->profitCalculator->calculate($player, $betAmount = 50, $winAmount = 0);
        $this->assertEquals(0, $realMoneyWallet->getCurrentValue());
    }

    public function testOnlyBonusMoneyWin()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 0);

        $bonusWallet = $this->createBonusWallet(20, 20, 0, 10);

        $player = new Player();
        $player->addWallet($realMoneyWallet);
        $player->addWallet($bonusWallet);

        $this->profitCalculator->calculate($player, $betAmount = 10, 0);
        $this->assertEquals(10, $bonusWallet->getCurrentValue());
        $this->assertEquals(0, $realMoneyWallet->getCurrentValue());

        $this->profitCalculator->calculate($player, $betAmount = 10, 0);
        $this->assertEquals(0, $bonusWallet->getCurrentValue());
        $this->assertEquals(0, $realMoneyWallet->getCurrentValue());
    }

    public function testBonusWinNotWagered()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 0);

        $bonusWallet = $this->createBonusWallet(20, 20, 0, 10);

        $player = new Player();
        $player->addWallet($realMoneyWallet);
        $player->addWallet($bonusWallet);

        $this->profitCalculator->calculate($player, $betAmount = 10, $winAmount = 30);
        $this->assertEquals(20, $bonusWallet->getCurrentValue());
        $this->assertEquals(0, $realMoneyWallet->getCurrentValue());
    }

    public function testBonusWinWagered()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 0);

        $bonusWallet = $this->createBonusWallet(20, 20, 120, 2);

        $player = new Player();
        $player->addWallet($realMoneyWallet);
        $player->addWallet($bonusWallet);

        $this->profitCalculator->calculate($player, $betAmount = 10, $winAmount = 30);
        $this->assertEquals(20, $bonusWallet->getCurrentValue());
        $this->assertEquals(20, $realMoneyWallet->getCurrentValue());
    }

    public function testWinFull()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 100);

        $bonusWallet = $this->createBonusWallet(10, 10, 150, 15);

        $player = new Player();
        $player->addWallet($realMoneyWallet);
        $player->addWallet($bonusWallet);

        $this->profitCalculator->calculate($player, $betAmount = 110, $winAmount = 120);
        $this->assertEquals(10, $bonusWallet->getCurrentValue());
        $this->assertEquals(110, $realMoneyWallet->getCurrentValue());
    }

    public function testOnlyMultipleBonusLost()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 0);

        $loginBonusWallet = $this->createBonusWallet(10, 10, 0, 15);

        $depositBonusWallet = $this->createBonusWallet(10, 10, 0, 15);

        $player = new Player();
        $player->addWallet($realMoneyWallet);
        $player->addWallet($loginBonusWallet);
        $player->addWallet($depositBonusWallet);

        $this->profitCalculator->calculate($player, $betAmount = 20, $winAmount = 0);
        $this->assertEquals(0, $loginBonusWallet->getCurrentValue());
        $this->assertEquals(0, $depositBonusWallet->getCurrentValue());
        $this->assertEquals(0, $realMoneyWallet->getCurrentValue());
    }

    public function testOnlyMultipleBonusWin()
    {
        $realMoneyWallet = $this->createRealMoneyWallet(100, 0);

        $loginBonusWallet = $this->createBonusWallet(10, 10, 150, 15);

        $depositBonusWallet = $this->createBonusWallet(10, 10, 0, 15);

        $player = new Player();
        $player->addWallet($realMoneyWallet);
        $player->addWallet($loginBonusWallet);
        $player->addWallet($depositBonusWallet);

        $this->profitCalculator->calculate($player, $betAmount = 20, $winAmount = 30);
        $this->assertEquals(10, $loginBonusWallet->getCurrentValue());
        $this->assertEquals(10, $depositBonusWallet->getCurrentValue());
        $this->assertEquals(10, $realMoneyWallet->getCurrentValue());
    }

    /**
     * @param $initialValue
     * @param $currentValue
     * @return Wallet
     */
    protected function createRealMoneyWallet($initialValue, $currentValue)
    {
        $realMoneyWallet = new Wallet();
        $realMoneyWallet->setInitialValue($initialValue);
        $realMoneyWallet->setCurrentValue($currentValue);
        $realMoneyWallet->setCurrency(CurrencyTypeEnum::EUR);
        $realMoneyWallet->setStatus(WalletStatusEnum::ACTIVE);

        return $realMoneyWallet;
    }

    /**
     * @param $currentValue
     * @param $initialValue
     * @param $wageredAmount
     * @param $multiplier
     * @return BonusWallet
     */
    protected function createBonusWallet($currentValue, $initialValue, $wageredAmount, $multiplier)
    {
        $bonusWallet = new BonusWallet();
        $bonusWallet->setStatus(WalletStatusEnum::ACTIVE);
        $bonusWallet->setCurrentValue($currentValue);
        $bonusWallet->setInitialValue($initialValue);
        $bonusWallet->setCurrency(CurrencyTypeEnum::BNS);

        $reward = new Reward();
        $reward->setValue(10);

        $bonus = new Bonus();
        $bonus->setWageringMultiplier($multiplier);
        $bonus->setReward($reward);
        $bonusWallet->setBonus($bonus);
        $bonusWallet->setWageredAmount($wageredAmount);

        return $bonusWallet;
    }
}