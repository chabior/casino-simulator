<?php

namespace CasinoBundle\Tests\Entity;

use CasinoBundle\Entity\Bonus;
use CasinoBundle\Entity\BonusWallet;
use CasinoBundle\Entity\Reward;
use CasinoBundle\Enum\RewardTypeEnum;
use CasinoBundle\Enum\WalletStatusEnum;
use PHPUnit\Framework\TestCase;

class BonusWalletTest extends TestCase
{
    /**
     * @var BonusWallet
     */
    protected $bonusWallet;

    public function setUp()
    {
        $this->bonusWallet = new BonusWallet();
        $this->bonusWallet->setBonus(new Bonus());
    }

    public function testSubtractExactMoney()
    {
        $this->bonusWallet->setCurrentValue(20);

        $rest = $this->bonusWallet->subtractMoney(20);

        $this->assertEquals(0, $this->bonusWallet->getCurrentValue());
        $this->assertEquals(0, $rest);
        $this->assertEquals(20, $this->bonusWallet->getWageredAmount());
    }

    public function testSubtractLessMoney()
    {
        $this->bonusWallet->setCurrentValue(20);

        $rest = $this->bonusWallet->subtractMoney(10);

        $this->assertEquals(10, $this->bonusWallet->getCurrentValue());
        $this->assertEquals(0, $rest);
        $this->assertEquals(10, $this->bonusWallet->getWageredAmount());
    }

    public function testSubtractMoreMoney()
    {
        $this->bonusWallet->setCurrentValue(20);

        $rest = $this->bonusWallet->subtractMoney(30);

        $this->assertEquals(0, $this->bonusWallet->getCurrentValue());
        $this->assertEquals(10, $rest);
        $this->assertEquals(20, $this->bonusWallet->getWageredAmount());
    }

    public function testMultipleSubtractWageredAmount()
    {
        $this->bonusWallet->setCurrentValue(40);

        $this->bonusWallet->subtractMoney(10);
        $this->bonusWallet->subtractMoney(20);

        $this->assertEquals(30, $this->bonusWallet->getWageredAmount());
    }

    public function testSubtractFromEmpty()
    {
        $this->bonusWallet->setCurrentValue(0);

        $rest = $this->bonusWallet->subtractMoney(10);

        $this->assertEquals(0, $this->bonusWallet->getCurrentValue());
        $this->assertEquals(10, $rest);
        $this->assertEquals(0, $this->bonusWallet->getWageredAmount());
    }

    public function testAddMoney()
    {
        $this->bonusWallet->setInitialValue(20);
        $this->bonusWallet->setCurrentValue(0);

        $rest = $this->bonusWallet->addMoney(20);
        $this->assertEquals(20, $this->bonusWallet->getCurrentValue());
        $this->assertEquals(0, $rest);
    }

    public function testAddMoreMoney()
    {
        $this->bonusWallet->setInitialValue(20);
        $this->bonusWallet->setCurrentValue(10);

        $rest = $this->bonusWallet->addMoney(20);
        $this->assertEquals(20, $this->bonusWallet->getCurrentValue());
        $this->assertEquals(10, $rest);
    }

    public function testWageredFixedBonus()
    {
        $reward = new Reward();
        $reward->setType(RewardTypeEnum::FIXED_AMOUNT);
        $reward->setValue(10);

        $bonus = new Bonus();
        $bonus->setReward($reward);
        $bonus->setWageringMultiplier(10);

        $this->bonusWallet->setBonus($bonus);
        $this->bonusWallet->setInitialValue(10);
        $this->bonusWallet->setWageredAmount(90);

        $this->bonusWallet->setCurrentValue(10);
        $this->bonusWallet->subtractMoney(10);
        $this->bonusWallet->addMoney(20);

        $this->assertEquals(WalletStatusEnum::WAGERED, $this->bonusWallet->getStatus());
    }

    public function testWageredPercentBonus()
    {
        $reward = new Reward();
        $reward->setType(RewardTypeEnum::PERCENTAGE);
        $reward->setValue(10);

        $bonus = new Bonus();
        $bonus->setReward($reward);
        $bonus->setWageringMultiplier(10);

        $this->bonusWallet->setBonus($bonus);
        $this->bonusWallet->setInitialValue(10);
        $this->bonusWallet->setWageredAmount(90);
        $this->bonusWallet->setCurrentValue(10);

        $this->bonusWallet->subtractMoney(10);
        $this->bonusWallet->addMoney(20);

        $this->assertEquals(WalletStatusEnum::WAGERED, $this->bonusWallet->getStatus());
    }

    public function testNotWagered()
    {
        $reward = new Reward();
        $reward->setValue(10);

        $bonus = new Bonus();
        $bonus->setReward($reward);
        $bonus->setWageringMultiplier(10);

        $this->bonusWallet->setBonus($bonus);
        $this->bonusWallet->setInitialValue(10);
        $this->bonusWallet->setWageredAmount(80);
        $this->bonusWallet->setCurrentValue(10);

        $this->bonusWallet->subtractMoney(10);
        $this->bonusWallet->addMoney(20);

        $this->assertNotEquals(WalletStatusEnum::WAGERED, $this->bonusWallet->getStatus());
    }

    public function testDepleted()
    {
        $this->bonusWallet->setInitialValue(10);
        $this->bonusWallet->setCurrentValue(10);

        $this->bonusWallet->subtractMoney(10);
        $this->bonusWallet->setDepletedIf();

        $this->assertEquals(WalletStatusEnum::DEPLETED, $this->bonusWallet->getStatus());
    }
}