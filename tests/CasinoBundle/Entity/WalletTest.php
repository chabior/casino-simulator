<?php

namespace CasinoBundle\Tests\Entity;

use CasinoBundle\Entity\Wallet;
use PHPUnit\Framework\TestCase;

class WalletTest extends TestCase
{
    /**
     * @var Wallet
     */
    protected $wallet;

    public function setUp()
    {
        $this->wallet = new Wallet();
    }

    public function testSubtractExactMoney()
    {
        $this->wallet->setCurrentValue(20);

        $rest = $this->wallet->subtractMoney(20);

        $this->assertEquals(0, $this->wallet->getCurrentValue());
        $this->assertEquals(0, $rest);
    }

    public function testSubtractLessMoney()
    {
        $this->wallet->setCurrentValue(20);

        $rest = $this->wallet->subtractMoney(10);

        $this->assertEquals(10, $this->wallet->getCurrentValue());
        $this->assertEquals(0, $rest);
    }

    public function testSubtractMoreMoney()
    {
        $this->wallet->setCurrentValue(20);

        $rest = $this->wallet->subtractMoney(30);

        $this->assertEquals(0, $this->wallet->getCurrentValue());
        $this->assertEquals(10, $rest);
    }

    public function testSubtractFromEmpty()
    {
        $this->wallet->setCurrentValue(0);

        $rest = $this->wallet->subtractMoney(10);

        $this->assertEquals(0, $this->wallet->getCurrentValue());
        $this->assertEquals(10, $rest);
    }

    public function testAddMoney()
    {
        $this->wallet->setInitialValue(20);
        $this->wallet->setCurrentValue(0);

        $rest = $this->wallet->addMoney(10);

        $this->assertEquals(10, $this->wallet->getCurrentValue());
        $this->assertEquals(0, $rest);
    }

    public function testAddMoreMoney()
    {
        $this->wallet->setInitialValue(20);
        $this->wallet->setCurrentValue(0);

        $rest = $this->wallet->addMoney(40);

        $this->assertEquals(40, $this->wallet->getCurrentValue());
        $this->assertEquals(0, $rest);
    }
}