<?php

namespace CasinoBundle\Tests\Entity;


use CasinoBundle\Entity\Bonus;
use PHPUnit\Framework\TestCase;

class BonusTest extends TestCase
{
    /**
     * @var Bonus
     */
    protected $bonus;

    public function setUp()
    {
        $this->bonus = new Bonus();
    }

    public function testIsWagered()
    {
        $this->bonus->setWageringMultiplier(10);

        $isWagered = $this->bonus->isWagered(100, 10);

        $this->assertTrue($isWagered);
    }

    public function testIsWageredMore()
    {
        $this->bonus->setWageringMultiplier(10);

        $isWagered = $this->bonus->isWagered(120, 10);

        $this->assertTrue($isWagered);
    }

    public function testIsNotWagered()
    {
        $this->bonus->setWageringMultiplier(10);

        $isWagered = $this->bonus->isWagered(50, 10);

        $this->assertFalse($isWagered);
    }
}