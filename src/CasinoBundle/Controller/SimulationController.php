<?php

namespace CasinoBundle\Controller;

use CasinoBundle\Entity\Player;
use CasinoBundle\Event\DepositEvent;
use CasinoBundle\Event\LoginEvent;
use CasinoBundle\Form\DepositForm;
use CasinoBundle\Form\GamePlayForm;
use CasinoBundle\Form\LoginForm;
use CasinoBundle\Form\PlayerChangeForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SimulationController extends Controller
{
    /**
     * @var Player
     */
    protected $currentPlayer;

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function engineAction()
    {
        $currentPlayer = $this->getCurrentPlayer();

        $loginForm = $this->createForm(LoginForm::class);

        $gamePlayForm = $this->createForm(GamePlayForm::class, null, [
            'player' => $currentPlayer
        ]);

        $playerChangeForm = $this->createForm(PlayerChangeForm::class, ['player' => $currentPlayer]);

        $depositForm = $this->createForm(DepositForm::class);

        return $this->render('CasinoBundle:Simulation:engine.html.twig', [
            'loginForm' => $loginForm->createView(),
            'gamePlayForm' => $gamePlayForm->createView(),
            'playerChangeForm' => $playerChangeForm->createView(),
            'depositForm' => $depositForm->createView(),
            'currentPlayer' => $currentPlayer
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function changePlayerAction(Request $request)
    {
        $playerChangeForm = $this->createForm(PlayerChangeForm::class);
        $playerChangeForm->handleRequest($request);

        if ($playerChangeForm->isValid()) {
            $this->currentPlayer = $playerChangeForm->get('player')->getData();

            $this->login($this->currentPlayer, $request);

            return new RedirectResponse($this->generateUrl('casino_engine'));
        }

        return $this->redirectWithErrors($playerChangeForm, $request);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function loginAction(Request $request)
    {
        $loginForm = $this->createForm(LoginForm::class);
        $loginForm->handleRequest($request);

        if ($loginForm->isValid()) {
            $currentPlayer = $this->getCurrentPlayer();
            $this->login($currentPlayer, $request);

            $this->get('event_dispatcher')->dispatch('casino.login', new LoginEvent(
                $currentPlayer
            ));

            return new RedirectResponse($this->generateUrl('casino_engine'));
        }

        return $this->redirectWithErrors($loginForm, $request);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function simulateDepositAction(Request $request)
    {
        $depositForm = $this->createForm(DepositForm::class);
        $depositForm->handleRequest($request);

        if ($depositForm->isValid()) {

            $this->get('event_dispatcher')->dispatch(
                'casino.deposit',
                new DepositEvent(
                    $depositForm->get('deposit')->getData(),
                    $this->getCurrentPlayer()
                )
            );

            return new RedirectResponse($this->generateUrl('casino_engine'));
        }

        return $this->redirectWithErrors($depositForm, $request);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function spinAction(Request $request)
    {
        $gamePlayForm = $this->createForm(GamePlayForm::class, null, [
            'player' => $this->getCurrentPlayer()
        ]);
        $gamePlayForm->handleRequest($request);

        if ($gamePlayForm->isValid()) {
            $simulator = $this->get('casino.game_simulator');
            $winMoney = $simulator->simulateSpin(
                $gamePlayForm->get('bet_amount')->getData(),
                $this->getCurrentPlayer()
            );

            if ($winMoney > 0) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', sprintf('Win! 10EUR'))
                ;
            } else {
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Lost!')
                ;
            }

            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('casino_engine'));
        }

        return $this->redirectWithErrors($gamePlayForm, $request);
    }

    /**
     * @param Player $player
     * @param Request|null $request
     */
    protected function login(Player $player, Request $request = null)
    {
        $token = new UsernamePasswordToken($player->getUsername(), null, "default", []);
        $this->get("security.token_storage")->setToken($token);

        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    }

    /**
     * @return Player|object
     */
    protected function getCurrentPlayer()
    {
        if (null === $this->currentPlayer) {;
            $token = $this->get('security.token_storage')->getToken();
            $username = $token->getUsername();
            $em = $this->getDoctrine()->getManager();
            $playerRepository = $em->getRepository('CasinoBundle:Player');

            //ugly hack for speeding
            if (empty($username) || 'anon.' == $username) {
                $this->currentPlayer = $playerRepository->findOneBy([]);
            } else {
                $this->currentPlayer = $playerRepository->findOneBy(['username' => $username]);
            }
        }

        return $this->currentPlayer;
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getFormErrors(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors(true, false) as $iterator) {
            foreach ($iterator as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $errors;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return RedirectResponse
     */
    protected function redirectWithErrors(FormInterface $form, Request $request)
    {
        $errors = $this->getFormErrors($form);

        $request->getSession()
            ->getFlashBag()
            ->add('success', sprintf('Errors! %s', json_encode($errors)))
        ;

        return new RedirectResponse($this->generateUrl('casino_engine'));
    }
}