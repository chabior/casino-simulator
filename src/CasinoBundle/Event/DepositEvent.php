<?php

namespace CasinoBundle\Event;


use CasinoBundle\Entity\Player;
use CasinoBundle\Entity\Wallet;
use Symfony\Component\EventDispatcher\Event;

class DepositEvent extends Event
{
    /**
     * @var float
     */
    protected $depositAmount;

    /**
     * @var Player
     */
    protected $player;

    /**
     *
     * @param float $depositAmount
     * @param Player $player
     */
    public function __construct($depositAmount, Player $player)
    {
        $this->depositAmount = $depositAmount;
        $this->player = $player;
    }

    /**
     * @return float
     */
    public function getDepositAmount()
    {
        return $this->depositAmount;
    }

    /**
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }
}