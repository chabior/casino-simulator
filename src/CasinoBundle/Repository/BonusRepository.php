<?php

namespace CasinoBundle\Repository;

use CasinoBundle\Enum\TriggerTypeEnum;
use Doctrine\ORM\EntityRepository;

class BonusRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findLoginBonuses()
    {
        return $this->findBy([
            'trigger' => TriggerTypeEnum::LOGIN
        ]);
    }

    /**
     * @return array
     */
    public function findDepositBonuses()
    {
        return $this->findBy([
            'trigger' => TriggerTypeEnum::DEPOSIT
        ]);
    }
}