<?php

namespace CasinoBundle;

use CasinoBundle\Calculator\Adder;
use CasinoBundle\Calculator\Subtract;
use CasinoBundle\Entity\Player;
use Doctrine\Common\Collections\ArrayCollection;

class ProfitCalculator
{
    /**
     * @param Player $player
     * @param float $betAmount
     * @param float $winAmount
     */
    public function calculate(Player $player, $betAmount, $winAmount = 0.0)
    {
        $usedOnlyRealMoney = $this->subtract($player, $betAmount);

        $this->add($player, $winAmount, $usedOnlyRealMoney);
    }

    /**
     * @param Player $player
     * @param $betAmount
     * @return bool
     */
    protected function subtract(Player $player, $betAmount)
    {
        $subtract = new Subtract();
        $subtractWallets = new ArrayCollection(
            [$player->getRealMoneyWallet()] +
            $player->getActiveBonusWallets()->toArray()
        );

        return $subtract->calculate($subtractWallets, $betAmount);
    }

    /**
     * @param Player $player
     * @param $winAmount
     * @param $usedOnlyRealMoney
     */
    protected function add(Player $player, $winAmount, $usedOnlyRealMoney)
    {
        $addWallets = $this->createAddWallets($player, $usedOnlyRealMoney);

        $adder = new Adder();
        $adder->calculate($addWallets, $winAmount);
    }

    /**
     * @param Player $player
     * @param $usedOnlyRealMoney
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    protected function createAddWallets(Player $player, $usedOnlyRealMoney)
    {
        if ($usedOnlyRealMoney) {
            $addWallets = new ArrayCollection([$player->getRealMoneyWallet()]);
        } else {
            $addWallets = $player->getSubtractedBonusWallets();
            if ($player->hasSubtractedWageredBonusWallets()) {
                $addWallets->add($player->getRealMoneyWallet());
            }
        }

        return $addWallets;
    }
}