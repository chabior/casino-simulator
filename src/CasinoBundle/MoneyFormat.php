<?php

namespace CasinoBundle;


class MoneyFormat
{
    /**
     * @param $value
     * @return string
     */
    public static function format($value)
    {
        return number_format(round($value, 2), 2);
    }
}