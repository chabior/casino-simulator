<?php

namespace CasinoBundle\DataFixtures\ORM;


use CasinoBundle\Entity\Player;
use CasinoBundle\Enum\GenderEnum;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPlayerData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $player = new Player();
        $player->setAge(21);
        $player->setGender(GenderEnum::MALE);
        $player->setLastName('Testowy');
        $player->setName('Tester');
        $player->setPassword(password_hash('b4RdZ0t4jn3h4sl0!', PASSWORD_BCRYPT));
        $player->setUsername('tester@test.com');

        $manager->persist($player);

        $player = new Player();
        $player->setAge(22);
        $player->setGender(GenderEnum::FEMALE);
        $player->setLastName('Demo');
        $player->setName('Demo');
        $player->setPassword(password_hash(uniqid(), PASSWORD_BCRYPT));
        $player->setUsername('demo@test.com');

        $manager->persist($player);
        $manager->flush();
    }
}