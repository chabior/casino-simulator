<?php

namespace CasinoBundle\DataFixtures\ORM;

use CasinoBundle\Entity\Bonus;
use CasinoBundle\Entity\Reward;
use CasinoBundle\Enum\RewardTypeEnum;
use CasinoBundle\Enum\TargetWalletTypeEnum;
use CasinoBundle\Enum\TriggerTypeEnum;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBonusData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $manager->persist($this->createLoginBonus());
        $manager->persist($this->createDepositBonus());
        $manager->flush();
    }

    /**
     * @return Bonus
     */
    protected function createLoginBonus()
    {
        $reward = new Reward();
        $reward->setTargetWallet(TargetWalletTypeEnum::BONUS);
        $reward->setType(RewardTypeEnum::FIXED_AMOUNT);
        $reward->setValue(10);

        $bonus = new Bonus();
        $bonus->setName('Login bonus');
        $bonus->setWageringMultiplier(1);
        $bonus->setReward($reward);
        $bonus->setTrigger(TriggerTypeEnum::LOGIN);

        return $bonus;
    }

    /**
     * @return Bonus
     */
    protected function createDepositBonus()
    {
        $reward = new Reward();
        $reward->setTargetWallet(TargetWalletTypeEnum::BONUS);
        $reward->setType(RewardTypeEnum::PERCENTAGE);
        $reward->setValue(10);

        $bonus = new Bonus();
        $bonus->setName('Deposit bonus');
        $bonus->setWageringMultiplier(1);
        $bonus->setReward($reward);
        $bonus->setTrigger(TriggerTypeEnum::DEPOSIT);

        return $bonus;
    }
}