<?php

namespace CasinoBundle\Entity;

use CasinoBundle\Exception\NotCountableBonus;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CasinoBundle\Repository\BonusRepository")
 * @ORM\Table(name="bonus")
 */
class Bonus
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @ORM\Column(name="name", type="string", length=21)
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Embedded(class="Reward")
     *
     * @var Reward
     */
    protected $reward;

    /**
     * @ORM\Column(name="wagering_multiplier", type="integer")
     *
     * @var int
     */
    protected $wageringMultiplier;

    /**
     * @ORM\Column(name="`trigger`", type="integer")
     *
     * @var int
     */
    protected $trigger;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Bonus
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Bonus
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Reward
     */
    public function getReward()
    {
        return $this->reward;
    }

    /**
     * @param Reward $reward
     * @return Bonus
     */
    public function setReward(Reward $reward)
    {
        $this->reward = $reward;
        return $this;
    }

    /**
     * @return int
     */
    public function getWageringMultiplier()
    {
        return $this->wageringMultiplier;
    }

    /**
     * @param int $wageringMultiplier
     * @return Bonus
     */
    public function setWageringMultiplier($wageringMultiplier)
    {
        $this->wageringMultiplier = $wageringMultiplier;
        return $this;
    }

    /**
     * @return int
     */
    public function getTrigger()
    {
        return $this->trigger;
    }

    /**
     * @param int $trigger
     * @return Bonus
     */
    public function setTrigger($trigger)
    {
        $this->trigger = $trigger;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasFixedReward()
    {
        return $this->reward->isFixed();
    }

    /**
     * @return int
     */
    public function getRewardValue()
    {
        return $this->reward->getValue();
    }

    /**
     * @param null|int $base
     * @return float|int
     * @throws NotCountableBonus when $base is required to calculate bonus
     */
    public function calculateInitialValue($base = null)
    {
        if (empty($base) && !$this->hasFixedReward()) {
            throw NotCountableBonus::get();
        }

        $bonus = $this->getRewardValue();
        if (!$this->hasFixedReward()) {
            $bonus = bcmul(bcdiv($bonus, 100, 4), $base, 4);
        }

        return $bonus;
    }

    /**
     * @param float $wageredAmount
     * @param float $initialValue
     * @return bool
     */
    public function isWagered($wageredAmount, $initialValue)
    {
        return in_array(
            bccomp(
                $wageredAmount,
                $this->getWageredRequiredAmount(
                    $initialValue
                ),
                4
            ),
            [1, 0]
        );
    }

    /**
     * @param $initialValue
     * @return string
     */
    public function getWageredRequiredAmount($initialValue)
    {
        return bcmul($this->getWageringMultiplier(), $initialValue, 4);
    }
}