<?php

namespace CasinoBundle\Entity;

use CasinoBundle\Enum\CurrencyTypeEnum;
use CasinoBundle\Enum\WalletStatusEnum;
use CasinoBundle\MoneyFormat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class BonusWallet extends Wallet
{
    /**
     *
     * @ORM\ManyToOne(targetEntity="Bonus")
     * @ORM\JoinColumn(name="bonus_id", referencedColumnName="id")
     *
     * @var Bonus
     */
    protected $bonus;

    /**
     *  @ORM\Column(name="wagered_amount", type="decimal", scale=4, precision=19)
     *
     * @var float
     */
    protected $wageredAmount;

    public function __construct()
    {
        $this->setCurrency(CurrencyTypeEnum::BNS);
    }

    /**
     * @return Bonus
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param Bonus $bonus
     * @return Wallet
     */
    public function setBonus(Bonus $bonus)
    {
        $this->bonus = $bonus;
        return $this;
    }

    /**
     * @return float
     */
    public function getWageredAmount()
    {
        return $this->wageredAmount;
    }

    /**
     * @return string
     */
    public function getFormattedWageredAmount()
    {
        return MoneyFormat::format($this->getWageredAmount());
    }

    /**
     * @param float $wageredAmount
     * @return Wallet
     */
    public function setWageredAmount($wageredAmount)
    {
        $this->wageredAmount = $wageredAmount;
        $this->setStatusToWageredIf();

        return $this;
    }

    /**
     * @param float $amount
     */
    public function addWageredAmount($amount)
    {
        $this->wageredAmount = bcadd($this->wageredAmount, $amount);

        $this->setStatusToWageredIf();
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return parent::isActive() || $this->getStatus() === WalletStatusEnum::WAGERED;
    }

    /**
     * @param float $amount
     * @return float
     */
    public function subtractMoney($amount)
    {
        $rest = parent::subtractMoney($amount);

        $this->addWageredAmount(bcsub($amount, $rest));

        return $rest;
    }

    /**
     * @param $amount
     * @return float
     */
    public function addMoney($amount)
    {
        $this->subtracted = false;
        $initialValue = $this->getInitialValue();
        $currentValue = $this->getCurrentValue();
        $adder = bcadd($amount, $currentValue, 4);
        $this->setCurrentValue(min($initialValue, $adder));

        return $initialValue >= $adder ? 0 : bcsub($adder, $initialValue);
    }

    /**
     * @return bool
     */
    public function isBonus()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isReal()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isWagered()
    {
        return $this->getStatus() === WalletStatusEnum::WAGERED;
    }

    /**
     * @return string
     */
    public function getFormattedWageredRequiredAmount()
    {
        return MoneyFormat::format($this->bonus->getWageredRequiredAmount($this->getInitialValue()));
    }

    /**
     * @return bool
     */
    protected function isBonusWagered()
    {
        return $this->bonus->isWagered(
            $this->getWageredAmount(),
            $this->getInitialValue()
        );
    }

    protected function setStatusToWageredIf()
    {
        if ($this->isBonusWagered()) {
            $this->setStatus(WalletStatusEnum::WAGERED);
        }
    }
}