<?php

namespace CasinoBundle\Entity;

use CasinoBundle\Exception\TooMuchRealMoneyWalletsException;
use CasinoBundle\MoneyFormat;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="player", uniqueConstraints={@ORM\UniqueConstraint(name="username_idx", columns={"username"})})
 */
class Player
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", length=21)
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(name="password", type="string", length=60)
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(name="name", type="string", length=21)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="last_name", type="string", length=21)
     * @var string
     */
    protected $lastName;

    /**
     * @ORM\Column(name="age", type="integer")
     * @var int
     */
    protected $age;

    /**
     * @ORM\Column(name="gender", type="integer")
     * @var int
     */
    protected $gender;

    /**
     * @ORM\OneToMany(targetEntity="Wallet", mappedBy="player", cascade={"persist", "remove"})
     *
     * @var Wallet[]|Collection
     */
    protected $wallets;

    public function __construct()
    {
        $this->wallets = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Player
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param int $username
     * @return Player
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return int
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Player
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Player
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return Player
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return Player
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return Wallet[]|Collection
     */
    public function getWallets()
    {
        return $this->wallets;
    }

    /**
     * @param Wallet[]|Collection $wallets
     * @return Player
     */
    public function setWallets(Collection $wallets)
    {
        $this->wallets = $wallets;
        $this->checkRealMoneyWallets();
    }

    /**
     * @param Wallet $wallet
     */
    public function addWallet(Wallet $wallet)
    {
        if ($wallet->isReal()) {
            $this->addRealMoneyWallet($wallet);
        } else {
            $wallet->setPlayer($this);
            $this->wallets->add($wallet);

            $this->checkRealMoneyWallets();
        }
    }

    /**
     * @param Wallet $wallet
     */
    public function addRealMoneyWallet(Wallet $wallet)
    {
        $realMoneyWallet = $this->getRealMoneyWallet();

        if (empty($realMoneyWallet)) {
            $wallet->setPlayer($this);
            $this->wallets->add($wallet);
        } else {
            $realMoneyWallet->addMoney($wallet->getInitialValue());
        }
    }

    /**
     * @return Wallet
     */
    public function getRealMoneyWallet()
    {
        $realMoneyWallet = $this->wallets->filter(function (Wallet $wallet) {
            return $wallet->isReal();
        });

        return $realMoneyWallet->first();
    }

    /**
     * @return Wallet
     */
    public function getFirstActiveBonusWallet()
    {
        $bonusWallet = $this->getActiveBonusWallets();

        return $bonusWallet->first();
    }

    /**
     * @return Collection|BonusWallet[]
     */
    public function getActiveBonusWallets()
    {
        return $this->wallets->filter(function (Wallet $wallet) {
            return $wallet->isBonus() && $wallet->isActive();
        });
    }

    /**
     * @return Collection
     */
    public function getSubtractedBonusWallets()
    {
        return $this->getActiveBonusWallets()->filter(function (Wallet $wallet) {
            return $wallet->isSubtracted();
        });
    }

    /**
     * @return bool
     */
    public function hasSubtractedWageredBonusWallets()
    {
        return (bool) $this->getSubtractedBonusWallets()->filter(function (Wallet $wallet) {
            return $wallet->isWagered();
        })->count();
    }

    /**
     * @return string
     */
    public function calculateTotalValue()
    {
        $totalValue = 0.0;
        foreach ($this->wallets as $wallet) {
            if ($wallet->isActive()) {
                $totalValue = bcadd($totalValue, $wallet->getCurrentValue());
            }
        }

        return MoneyFormat::format($totalValue);
    }

    /**
     * @return string
     */
    public function getRealMoneyValue()
    {
        $realMoneyWallet = $this->getRealMoneyWallet();

        if (empty($realMoneyWallet)) {
            return MoneyFormat::format(0);
        }

        return MoneyFormat::format($realMoneyWallet->getCurrentValue());
    }

    /**
     * Checks when bonus wallets been depleted
     */
    public function checkDepletedWallets()
    {
        foreach ($this->getActiveBonusWallets() as $wallet) {
            $wallet->setDepletedIf();
        }
    }

    /**
     * @throws TooMuchRealMoneyWalletsException
     */
    protected function checkRealMoneyWallets()
    {
        $realMoneyWallets = $this->wallets->filter(function (Wallet $wallet) {
            return $wallet->isReal();
        });

        if ($realMoneyWallets->count() > 1) {
            throw TooMuchRealMoneyWalletsException::get();
        }
    }
}