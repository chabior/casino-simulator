<?php

namespace CasinoBundle\Entity;

use CasinoBundle\Enum\RewardTypeEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class Reward
{
    /**
     * @ORM\Column(name="type", type="integer")
     *
     * @var int
     */
    protected $type;

    /**
     * @ORM\Column(name="target_wallet", type="integer")
     *
     * @var integer
     */
    protected $targetWallet;

    /**
     * @ORM\Column(name="value", type="integer")
     *
     * @var int
     */
    protected $value;

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Reward
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getTargetWallet()
    {
        return $this->targetWallet;
    }

    /**
     * @param int $targetWallet
     * @return Reward
     */
    public function setTargetWallet($targetWallet)
    {
        $this->targetWallet = $targetWallet;
        return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return Reward
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFixed()
    {
        return RewardTypeEnum::FIXED_AMOUNT === $this->getType();
    }
}