<?php

namespace CasinoBundle\Entity;

use CasinoBundle\Enum\CurrencyTypeEnum;
use CasinoBundle\Enum\WalletStatusEnum;
use CasinoBundle\MoneyFormat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="wallet")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="currency", type="integer")
 * @ORM\DiscriminatorMap({1 = "Wallet", 0 = "BonusWallet"})
 */
class Wallet
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $currency;

    /**
     * @ORM\Column(name="initial_value", type="decimal", scale=4, precision=19)
     *
     * @var int
     */
    protected $initialValue;

    /**
     * @ORM\Column(name="current_value", type="decimal", scale=4, precision=19)
     *
     * @var int
     */
    protected $currentValue;

    /**
     * @ORM\Column(name="status", type="integer")
     *
     * @var int
     */
    protected $status;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="wallets")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     *
     * @var Player
     */
    protected $player;

    /**
     * @var bool
     */
    protected $subtracted = false;

    public function __construct()
    {
        $this->setCurrency(CurrencyTypeEnum::EUR);
    }

    /**
     * @return float
     */
    public function getInitialValue()
    {
        return $this->initialValue;
    }

    /**
     * @param float $initialValue
     * @return $this
     */
    public function setInitialValue($initialValue)
    {
        $this->initialValue = $initialValue;

        return $this;
    }

    /**
     * @return float
     */
    public function getCurrentValue()
    {
        return $this->currentValue;
    }

    /**
     * @return string
     */
    public function getFormattedCurrentValue()
    {
        return MoneyFormat::format($this->getCurrentValue());
    }

    /**
     * @param float $currentValue
     * @return $this
     */
    public function setCurrentValue($currentValue)
    {
        $this->currentValue = $currentValue;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Wallet
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     * @return Wallet
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Wallet
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param Player $player
     * @return Wallet
     */
    public function setPlayer(Player $player)
    {
        $this->player = $player;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
       return $this->getStatus() === WalletStatusEnum::ACTIVE;
    }

    /**
     * @return bool
     */
    public function isBonus()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isReal()
    {
        return true;
    }

    /**
     * @param float $amount
     * @return float
     */
    public function subtractMoney($amount)
    {
        $this->subtracted = true;
        $currentValue = $this->getCurrentValue();

        $this->setCurrentValue(max(0, bcsub($currentValue, $amount, 4)));

        return bccomp($amount, $currentValue) == 1 ? bcsub($amount, $currentValue, 4) : 0;
    }

    public function setDepletedIf()
    {
        if (bccomp($this->getCurrentValue(), 0) == 0) {
            $this->setStatus(WalletStatusEnum::DEPLETED);
        }
    }

    /**
     * @param $amount
     * @return int
     */
    public function addMoney($amount)
    {
        $this->subtracted = false;
        $this->setStatus(WalletStatusEnum::ACTIVE);
        $this->setCurrentValue(bcadd($this->getCurrentValue(), $amount));
    }

    /**
     * @return bool
     */
    public function isWagered()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isSubtracted()
    {
        return $this->subtracted;
    }
}