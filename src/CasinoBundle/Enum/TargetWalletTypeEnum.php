<?php

namespace CasinoBundle\Enum;


class TargetWalletTypeEnum
{
    const BONUS = 0;

    const REAL = 1;
}