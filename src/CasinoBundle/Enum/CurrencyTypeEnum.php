<?php

namespace CasinoBundle\Enum;


class CurrencyTypeEnum
{
    /**
     * Bonus
     */
    const BNS = 0;

    /**
     * Euro
     */
    const EUR = 1;

    /**
     * @var array
     */
    public static $all = [
        self::BNS,
        self::EUR
    ];
}