<?php

namespace CasinoBundle\Enum;


class RewardTypeEnum
{
    const PERCENTAGE = 0;

    const FIXED_AMOUNT = 1;
}