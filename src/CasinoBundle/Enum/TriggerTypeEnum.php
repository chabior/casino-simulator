<?php

namespace CasinoBundle\Enum;


class TriggerTypeEnum
{
    const LOGIN = 0;

    const DEPOSIT = 1;
}