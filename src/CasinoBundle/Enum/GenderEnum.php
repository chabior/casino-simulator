<?php

namespace CasinoBundle\Enum;


class GenderEnum
{
    const MALE = 0;

    const FEMALE = 1;
}