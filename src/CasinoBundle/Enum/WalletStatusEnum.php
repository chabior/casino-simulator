<?php

namespace CasinoBundle\Enum;


class WalletStatusEnum
{
    const ACTIVE = 0;

    const WAGERED = 1;

    const DEPLETED = 2;
}