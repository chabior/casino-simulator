<?php

namespace CasinoBundle\Calculator;

use CasinoBundle\Entity\Wallet;
use Doctrine\Common\Collections\Collection;

class Adder
{
    /**
     * @param Collection|Wallet[] $wallets
     * @param $amount
     */
    public function calculate(Collection $wallets, $amount)
    {
        if ($amount <= 0) {
            return ;
        }

        foreach ($wallets as $wallet) {
            $amount = $wallet->addMoney($amount);

            if ($amount == 0) {
                break;
            }
        }
    }
}