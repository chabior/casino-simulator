<?php

namespace CasinoBundle\Calculator;

use CasinoBundle\Entity\Wallet;
use Doctrine\Common\Collections\Collection;

class Subtract
{
    /**
     * @param Collection|Wallet[] $wallets
     * @param $amount
     * @return bool true if only real money was used
     */
    public function calculate(Collection $wallets, $amount)
    {
        $counter = 0;
        foreach ($wallets as $wallet) {
            ++$counter;
            $amount = $wallet->subtractMoney($amount);

            if ($amount <= 0) {
                break;
            }
        }

        return $counter === 1;
    }
}