<?php

namespace CasinoBundle;


use CasinoBundle\Entity\Player;

class GameSimulator
{
    /**
     * @var RandomSpinner
     */
    protected $spinner;

    /**
     * @var ProfitCalculator
     */
    protected $profitCalculator;

    /**
     *
     * @param RandomSpinner $spinner
     * @param ProfitCalculator $profitCalculator
     */
    public function __construct(RandomSpinner $spinner, ProfitCalculator $profitCalculator)
    {
        $this->spinner = $spinner;
        $this->profitCalculator = $profitCalculator;
    }

    /**
     * @param $betAmount
     * @param Player $player
     * @return int amount of win money
     */
    public function simulateSpin($betAmount, Player $player)
    {
        $isWin = $this->spinner->spin();

        $winAmount = !$isWin ? 0 : $betAmount + 10;
        $this->profitCalculator->calculate(
            $player,
            $betAmount,
            $winAmount
        );

        $player->checkDepletedWallets();

        return $winAmount;
    }
}