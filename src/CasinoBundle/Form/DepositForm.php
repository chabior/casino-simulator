<?php

namespace CasinoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\Range;

class DepositForm extends AbstractType
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('deposit', NumberType::class, [
            'label' => 'Amount to deposit',
            'constraints' => [
                new Range([
                    'min' => 10
                ])
            ]
        ]);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Deposit'
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('action', $this->router->generate('casino_simulate_deposit'));
    }
}