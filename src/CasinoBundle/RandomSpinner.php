<?php

namespace CasinoBundle;

class RandomSpinner
{
    /**
     * @var int
     */
    protected $factor = 2;

    /**
     * @return bool true when win
     */
    public function spin()
    {
        return mt_rand(0, $this->factor) === 0;
    }
}