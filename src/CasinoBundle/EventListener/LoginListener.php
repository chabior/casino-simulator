<?php

namespace CasinoBundle\EventListener;


use CasinoBundle\BonusApplicator;
use CasinoBundle\Event\LoginEvent;
use CasinoBundle\Repository\BonusRepository;
use Doctrine\ORM\EntityManagerInterface;

class LoginListener
{
    /**
     * @var BonusRepository
     */
    protected $bonusRepository;

    /**
     * @var BonusApplicator
     */
    protected $bonusApplicator;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     *
     * @param BonusRepository $bonusRepository
     * @param BonusApplicator $bonusApplicator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        BonusRepository $bonusRepository,
        BonusApplicator $bonusApplicator,
        EntityManagerInterface $entityManager
    ) {
        $this->bonusRepository = $bonusRepository;
        $this->bonusApplicator = $bonusApplicator;
        $this->entityManager = $entityManager;
    }


    /**
     * @param LoginEvent $interactiveLoginEvent
     */
    public function onLogin(LoginEvent $interactiveLoginEvent)
    {
        $bonuses = $this->bonusRepository->findLoginBonuses();
        $player = $interactiveLoginEvent->getPlayer();

        $this->bonusApplicator->apply($bonuses, $player);

        $this->entityManager->persist($player);
        $this->entityManager->flush();
    }
}