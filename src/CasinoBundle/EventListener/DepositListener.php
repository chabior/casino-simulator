<?php

namespace CasinoBundle\EventListener;


use CasinoBundle\BonusApplicator;
use CasinoBundle\Entity\Wallet;
use CasinoBundle\Enum\CurrencyTypeEnum;
use CasinoBundle\Enum\WalletStatusEnum;
use CasinoBundle\Event\DepositEvent;
use CasinoBundle\Repository\BonusRepository;
use Doctrine\ORM\EntityManagerInterface;

class DepositListener
{
    /**
     * @var BonusRepository
     */
    protected $bonusRepository;

    /**
     * @var BonusApplicator
     */
    protected $bonusApplicator;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     *
     * @param BonusRepository $bonusRepository
     * @param BonusApplicator $bonusApplicator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        BonusRepository $bonusRepository,
        BonusApplicator $bonusApplicator,
        EntityManagerInterface $entityManager
    ) {
        $this->bonusRepository = $bonusRepository;
        $this->bonusApplicator = $bonusApplicator;
        $this->entityManager = $entityManager;
    }


    /**
     * @param DepositEvent $depositEvent
     */
    public function onDeposit(DepositEvent $depositEvent)
    {
        $bonuses = $this->bonusRepository->findDepositBonuses();
        $player = $depositEvent->getPlayer();

        $this->bonusApplicator->apply(
            $bonuses,
            $player,
            $depositEvent->getDepositAmount()
        );

        $wallet = new Wallet();
        $wallet->setInitialValue($depositEvent->getDepositAmount());
        $wallet->setCurrentValue($depositEvent->getDepositAmount());
        $wallet->setStatus(WalletStatusEnum::ACTIVE);
        $player->addWallet($wallet);

        $this->entityManager->persist($player);
        $this->entityManager->flush();
    }
}