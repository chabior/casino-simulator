<?php

namespace CasinoBundle\Exception;


class UnsupportedCurrencyException extends Exception
{

    /**
     * @param $currency
     * @param array $supported
     * @return UnsupportedCurrencyException
     */
    public static function get($currency, array $supported)
    {
        return new static(sprintf(
            'Currency %s is not supported. Supported currencies %s.',
            $currency,
            json_encode($supported)
        ));
    }
}