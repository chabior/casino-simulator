<?php

namespace CasinoBundle\Exception;


class NotCountableBonus extends Exception
{
    /**
     * @return NotCountableBonus
     */
    public static function get()
    {
        return new static(sprintf('Bonus with percentage reward need base value.'));
    }
}