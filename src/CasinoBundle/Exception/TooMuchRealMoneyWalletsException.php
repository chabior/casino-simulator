<?php

namespace CasinoBundle\Exception;


class TooMuchRealMoneyWalletsException extends Exception
{
    /**
     * @return TooMuchRealMoneyWalletsException
     */
    public static function get()
    {
        return new static(sprintf('Player can have only one real money wallet!'));
    }
}