<?php

namespace CasinoBundle;

use CasinoBundle\Entity\Bonus;
use CasinoBundle\Entity\BonusWallet;
use CasinoBundle\Entity\Player;
use CasinoBundle\Enum\WalletStatusEnum;

class BonusApplicator
{
    /**
     * @param array|Bonus[] $bonuses
     * @param Player $player
     * @param null $baseValue
     */
    public function apply(array $bonuses, Player $player, $baseValue = null)
    {
        foreach ($bonuses as $bonus) {
            $wallet = new BonusWallet();
            $initialValue = $bonus->calculateInitialValue($baseValue);
            $wallet->setInitialValue($initialValue);
            $wallet->setCurrentValue($initialValue);
            $wallet->setBonus($bonus);
            $wallet->setWageredAmount(0);
            $wallet->setStatus(WalletStatusEnum::ACTIVE);
            $player->addWallet($wallet);
        }
    }
}